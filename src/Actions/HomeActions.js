export const userDataAction=()=>{
    return dispatch=>{
        const url="https://jsonplaceholder.typicode.com/users"
        fetch(url,{
            method:"GET",
            // headers: {
            //     'Content-Type': 'application/json'
            // }
        })
        .then(response=>response.json())
        .then(result=>{
            if(result){
                dispatch({type:'USER_DATA',payload:result})
            }
            else{
                console.log("somthing worng in fetch")
            }
        })
    }
}
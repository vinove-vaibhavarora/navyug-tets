export const PostDetailAction=(id)=>{
    // console.log(id,"id in pos")
    return dispatch=>{
        const url=`https://jsonplaceholder.typicode.com/comments`
        fetch(url,{
            method:"GET",
            // headers: {
            //     'Content-Type': 'application/json'
            // }
        })
        .then(response=>response.json())
        .then(result=>{
            if(result){
                // console.log(result,"res")
                dispatch({type:'SHOW_COMMENTS',payload:result})
            }
            else{
                console.log("somthing worng in fetch")
            }
        })
    }
}


export const OnSubmitAction=(body)=>{
    return dispatch=>{
        const url=`https://jsonplaceholder.typicode.com/posts`
        fetch(url,{
            method:"POST",
            body:body
            // headers: {
            //     'Content-Type': 'application/json'
            // }
        })
        .then(response=>response.json())
        .then(result=>{
            
                // console.log(result,"res")
                return result;
                // dispatch({type:'USER_POSTS',payload:result})
            
        })
    }
}
export const UserPostsAction=(id)=>{
    // console.log(id,"id in pos")
    return dispatch=>{
        const url=`https://jsonplaceholder.typicode.com/posts?userId=${id}`
        fetch(url,{
            method:"GET",
            // headers: {
            //     'Content-Type': 'application/json'
            // }
        })
        .then(response=>response.json())
        .then(result=>{
            if(result){
                // console.log(result,"res")
                dispatch({type:'USER_POSTS',payload:result})
            }
            else{
                console.log("somthing worng in fetch")
            }
        })
    }
}


export const CreatePostsAction=(body)=>{
    // console.log(body,"body in pos")
    return dispatch=>{
        const url=`https://jsonplaceholder.typicode.com/posts`
        fetch(url,{
            method:"POST",
            body:body
            // headers: {
            //     'Content-Type': 'application/json'
            // }
        })
        .then(response=>response.json())
        .then(result=>{
            
                console.log(result,"res")
                // dispatch({type:'USER_POSTS',payload:result})
            
        })
    }
}


export const DeletePostsAction=(id)=>{
    // console.log(body,"body in pos")
    return dispatch=>{
        const url=`https://jsonplaceholder.typicode.com/posts/${id}`
        fetch(url,{
            method:"DELETE"
        })
        .then(response=>response.json())
        .then(result=>{
            
                console.log(result,"res")
                console.log("deleted")
                // dispatch({type:'USER_POSTS',payload:result})
                return result
            
            // else{
            //     console.log("somthing worng in fetch")
            // }
        })
    }
}

export const InfoBody=(data)=>{
    // console.log(data,"herer")
    return dispatch =>
    dispatch({
        type:"USER_BODY",
        payload:data
    })
}
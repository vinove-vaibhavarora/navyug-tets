import React from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import Home from './Home/Home';
import UserDetails from './UserPosts/UserPosts'
import PostsDetails from './PostsDetail/PostDetails'


export const App=()=>{
    return(
        <Router>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route exact path='/user/:id' component={UserDetails}/>
                <Route exact path='/user/:id/:pid' component={PostsDetails}/>
            </Switch>
        </Router>
    )
}
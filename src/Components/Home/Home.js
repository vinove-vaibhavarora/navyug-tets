import React from 'react';
import { connect } from 'react-redux';
import { userDataAction } from '../../Actions/HomeActions';
import { Container } from 'react-bootstrap';
import UserCards from './UserCards'
class Home extends React.Component {
    state = {
        userData: []
    }
    componentDidMount() {
        this.props.userDataAction();
    }
    componentDidUpdate(preProps) {
        if (preProps.data !== this.props.data) {
            this.setState({ userData: this.props.data })
        }
    }
    render() {
        const { userData } = this.state;
        // console.log(userData, "here")
        return (
            <Container>
                <UserCards userData={userData} {...this.props}/>
            </Container>
        )
    }
}

const mapStateToProps = ({ HomeReducer }) => {
    return {
        data: HomeReducer.data
    }
}

export default connect(mapStateToProps, { userDataAction })(Home);
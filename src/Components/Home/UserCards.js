import React from 'react';
import { Card, Button, Row, Col, CardDeck } from 'react-bootstrap';
import {Link} from 'react-router-dom'

class UserCards extends React.Component {


    userDetail=(id,name)=>{
        // this.props.histor.push(`/user/${id}`)
        // console.log(id,name)
        this.props.history.push({pathname:`/user/${id}`,state:{name}})
    }

    render() {
        const { userData } = this.props;
        // console.log(this.props, "propsinhome")
        return (
            <Row>
                <CardDeck>
                {userData.length ? userData.map(user => (
                    <Col key={user.id} sm={3} style={{ padding: "10px" }}>
                        
                            <Card className="userCard">
                                <Card.Body>
                                    <Card.Title>{user.name}</Card.Title>
                                    <div>
                                        <Button variant="link">{user.email}</Button><br/>
                                        <Button variant="link">{user.phone}</Button><br/>
                                        <Button variant="link">{user.website}</Button><br/>
                                        
                                    </div>
                                    <div className="userComanyData">
                                        {user.company.name}<br/>
                                        {/* {user.company.catchPhrase}<br/> */}
                                        {user.company.catchPhrase.length >= 15 ? `${user.company.catchPhrase.slice(0,15)}...`: user.company.catchPhrase}<br/>
                                        {user.company.bs}<br/>
                                    </div>
                                    {/* <Link to={`/user/${user.id}`}  */}
                                    <Button className="userDetailBtn" onClick={()=>this.userDetail(user.id,user.name)} variant="primary">Details</Button>
                                </Card.Body>
                            </Card>
                    </Col>
                )) : null}
                </CardDeck>
            </Row>
        )
    }

}

export default UserCards
import React, { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap'

export function AddCommentModal(props) {
    const { modalState, modalFun, onSubmitComment } = props;
    // console.log(props, "addModa")
    const [state, setState] = useState({ name: "",email:"", body: "" })

    const onChangeText = (e) => {
        //    console.log(e.target.name)
        setState({ ...state, [e.target.name]: e.target.value })
    }

    const submit = () => {
        onSubmitComment(state)
    }

    //    useEffect(() => {
    //        console.log(state)
    //    }, [state])

    return (
        <>
            <Modal show={modalState} onHide={modalFun}>
                <Modal.Header closeButton>
                    <Modal.Title>Add Comment</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" name='name' onChange={onChangeText} placeholder="Enter Title" />
                        </Form.Group>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" name="email" onChange={onChangeText}  placeholder="Enter email" />
                            
                        </Form.Group>


                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Body</Form.Label>
                            <Form.Control as="textarea" name='body' onChange={onChangeText} placeholder="Enter Body" />
                        </Form.Group>
                    </Form>

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={modalFun}>
                        Close
            </Button>
                    <Button variant="primary" onClick={submit}>
                        Save
            </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}


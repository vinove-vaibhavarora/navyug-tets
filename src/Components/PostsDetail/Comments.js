import React from 'react';
import { Card, Button, Row, Col, CardDeck } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { InfoBody } from '../../Actions/UserPostsActions';

class Comments extends React.Component {

    // postInfo=(id,userId,body)=>{
    //     console.log(id,userId);
    //     this.props.history.push({pathname:`/user/${userId}/${id}`,state:this.props.userName});
    //     InfoBody(body)
    // }

    render() {
        const { data } = this.props;
        // console.log(data, "hre")
        return (
            <Row>
                <Col>
                    {
                        data.length ? data.map(com => (
                            <Card key={com.id} style={{marginBottom:"10px"}} >
                                
                                <Card.Body>
                                    <Row>
                        <Col>
                       <strong>{com.name}</strong> 
                        </Col>
                        <Col>
                        <Link to="#" style={{float:"right"}}> 
                        {com.email}
                        </Link>
                        </Col>
                                    </Row>
                                    {com.body}
                                    {/* {post.title.length >= 80 ? `${post.title.slice(0,80)}...`: post.title} */}
                                    </Card.Body>
                            </Card>
                        )) : null
                    }
                </Col>
            </Row>
        )
    }

}

export default Comments;
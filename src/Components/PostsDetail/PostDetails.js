import React from 'react';
import {connect} from 'react-redux'
import { Container, Button, Row, Col, CardDeck } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {PostDetailAction,OnSubmitAction} from '../../Actions/PostDetailActions';
import {AddCommentModal} from './AddCommentModal';
import Comments from './Comments'

class PostDetails extends React.Component{

    state={
        addCommentModal:false,
        showComments:false,
        comments:[]
    }


    addCommentHandler=()=>{
        this.setState({addCommentModal:!this.state.addCommentModal})
    }

    showCommentsHandler=()=>{
        this.setState({showComments:!this.state.showComments},()=>{
            this.props.PostDetailAction()
        })
    }
    componentDidMount(){
        
    }
    componentDidUpdate(preProps){
        if(preProps.comments !== this.props.comments){
            
            this.setState({comments:this.props.comments})
        }
    }
    onSubmitPost=async(data)=>{
        // console.log(data)
        if(!data.name || !data.email || !data.body){
          return  console.log("missing data")
        }
      let res= await this.props.OnSubmitAction(data)
      console.log("Submited")

    }
    render(){
        // console.log(this.state.comments,"post",this.props)
        return(
            <Container>
                <Row style={{padding:"50px"}}>
                    <Col sm={4}>
                    <Link to='/'>Back</Link>
                    </Col>
                    <Col sm={8}>
                   <h2> {this.props.location && this.props.location.state}</h2>
                    </Col>
                </Row>
                <Row style={{margin:"20px"}}>
                    <Col>
                    {this.props.body}
                    </Col>
                </Row>
                <Row>
                <Col>
                <Button variant="link" onClick={this.showCommentsHandler}>{this.state.showComments?"Hide Comments": "Show Comments"}</Button>
                </Col>
                <Col>
                <Button variant="link" style={{float:"right"}} onClick={this.addCommentHandler}>Add Comment</Button>
                </Col>
                </Row>
                {
                    this.state.addCommentModal ?<AddCommentModal modalState={this.state.addCommentModal} modalFun={this.addCommentHandler}
                    onSubmitComment={this.onSubmitPost}
                    /> : null
                }
                <div style={{marginTop:"10px"}}>
                {
                    this.state.showComments?<Comments data={this.state.comments}/> : null
                }
                </div>
            </Container>
        )
    }
}

const mapStateToProps = ({ PostDetailReducer }) => {
    // console.log(PostDetailReducer,"reddd")
    return {
        comments: PostDetailReducer.comments,
        body:PostDetailReducer.body
    }
}

export default connect(mapStateToProps,{PostDetailAction,OnSubmitAction})(PostDetails) ;
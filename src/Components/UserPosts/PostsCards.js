import React from 'react';
import {connect} from "react-redux"
import { Card, Button, Row, Col, CardDeck } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { InfoBody } from '../../Actions/UserPostsActions';

class PostsCards extends React.Component {

    postInfo=(id,userId,body)=>{
        // console.log(id,userId);
        this.props.history.push({pathname:`/user/${userId}/${id}`,state:this.props.userName});
        this.props.InfoBody(body)
    }

    render() {
        const { posts } = this.props;
        // console.log(this.props, "hre")
        return (
            <Row>
                <Col>
                    {
                        posts.length ? posts.map(post => (
                            <Card key={post.id} style={{marginBottom:"10px"}} >
                                
                                <Card.Body>
                                <Button variant="danger" onClick={()=>this.props.onDelete(post.id)}>Delete</Button>{"           "}
                                    {post.title.length >= 80 ? `${post.title.slice(0,80)}...`: post.title}
                                    <Button variant="info" style={{float:"right"}} onClick={()=>this.postInfo(post.id,post.userId,post.body)}>Info</Button>{' '}
                                    </Card.Body>
                            </Card>
                        )) : null
                    }
                </Col>
            </Row>
        )
    }

}

export default connect(null,{InfoBody})(PostsCards) ;
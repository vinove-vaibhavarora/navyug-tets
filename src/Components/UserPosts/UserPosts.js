import React from 'react';
import { connect } from 'react-redux';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { UserPostsAction, CreatePostsAction, DeletePostsAction } from '../../Actions/UserPostsActions';
import PostsCards from './PostsCards';
import {Link} from 'react-router-dom';
import {AddModal} from './AddModal';

class UserPosts extends React.Component {
    state = {
        posts: [],
        addPostModal:false,
    }
    componentDidMount() {
        let id = this.props.match.params.id;
        // console.log(id,"id")
        this.props.UserPostsAction(id)
    }
    componentDidUpdate(preProps) {
        if (preProps.posts !== this.props.posts) {
            this.setState({ posts: this.props.posts })
        }
    }

    addPostHandler=()=>{
        this.setState({addPostModal:!this.state.addPostModal})
    }

    onSubmitPost=(data)=>{
        if(!data.title || !data.body){
            return console.log("mising data")
        }
        let body={
            title:data.title,
            body:data.body
        }
        this.props.CreatePostsAction(body)
    }

    onDeletePost=async(id)=>{
        // console.log(id,"del")
     let result= await this.props.DeletePostsAction(id)
    //  console.log(result,"rses")
     if(result){
        let id = this.props.match.params.id;
        // console.log(id,"id")
        this.props.UserPostsAction(id)
        console.log("deleted")
     }
    }

    render() {
        const {posts}=this.state
        // console.log(posts,"props",this.props)
        return (
            <Container>
                <Row style={{padding:"50px"}}>
                    <Col>
                    <Link to='/'>Back</Link>
                    </Col>
                    <Col>
                   <h2> {this.props.location.state && this.props.location.state.name}</h2>
                    </Col>
                    <Col>
                    <Button variant="link" style={{float:"right"}} onClick={this.addPostHandler}>Add</Button>
                    </Col>
                </Row>
                <PostsCards posts={posts} onDelete={this.onDeletePost} userName={this.props.location.state && this.props.location.state.name} {...this.props}/>
                {
                    this.state.addPostModal ?<AddModal modalState={this.state.addPostModal} modalFun={this.addPostHandler}
                    onSubmitPost={this.onSubmitPost}
                    /> : null
                }
            </Container>
        )
    }
}

const mapStateToProps = ({ UserPostsReducer }) => {
    return {
        posts: UserPostsReducer.userPosts
    }
}

export default connect(mapStateToProps, { UserPostsAction,CreatePostsAction,DeletePostsAction })(UserPosts);
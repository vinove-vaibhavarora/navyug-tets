const initState={
    postDetail:[],
    comments:[],
    body:""
}


export const PostDetailReducer=(state=initState,{type,payload})=>{
    // console.log(payload,"ok here")
    switch(type){
        case "POST_DETAIL":
            return{
                ...state,
                postDetail:payload
            }
        case "SHOW_COMMENTS":
            return{
                ...state,
                comments:payload
            }
        case "USER_BODY":{
            return{
                ...state,
                body:payload
            }
        } 
            default:
                return{
                    ...state
                }
    }
}
import {combineReducers} from 'redux';
import {HomeReducer} from './HomeReducer';
import {UserPostsReducer} from './UserPostsReducer';
import {PostDetailReducer} from './PostDetailReducer'

export const RootReducer=combineReducers({
    HomeReducer,
    UserPostsReducer,
    PostDetailReducer
})
import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './Components/App';
import {Provider} from 'react-redux';
import {store} from './Store';
import "bootstrap/dist/css/bootstrap.min.css"
import "./Assets/Css/developer.css"

ReactDOM.render(
    <Provider  store={store}>
            <App/>
    </Provider>
,document.getElementById('root'));